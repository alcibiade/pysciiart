from pysciiart.graph import Graph
from pysciiart.widget import Paragraph, Border, VBox

result = ('+- Parent 1 ----------+     +- Parent 2 ----------+\n'
          '| +- Child A -------+ |     | +- Child C -------+ |\n'
          '| | Content Item A1 | |     | | Content Item C1 | |\n'
          '| +-----------------+ |---->| +-----------------+ |\n'
          '| +- Child B -------+ |     | +- Child D -------+ |\n'
          '| | Content Item B1 | |     | | Content Item D1 | |\n'
          '| +-----------------+ |     | +-----------------+ |\n'
          '+---------------------+     +---------------------+\n')


def run():
    c_a = Border(Paragraph(['Content Item A1']), 'Child A')
    c_b = Border(Paragraph(['Content Item B1']), 'Child B')
    p1 = Border(VBox([c_a, c_b]), 'Parent 1')

    c_c = Border(Paragraph(['Content Item C1']), 'Child C')
    c_d = Border(Paragraph(['Content Item D1']), 'Child D')
    p2 = Border(VBox([c_c, c_d]), 'Parent 2')

    model = Graph([p1, p2], [(c_b, c_c)])
    return model


if __name__ == '__main__':
    print(run().render())
