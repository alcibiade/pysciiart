from pysciiart.graph import Graph
from pysciiart.widget import Paragraph, Padding, Size, Border

result = ('+----------------+         +----------------+\n'
          '|                |         |                |\n'
          '|   Consumer A   |-------->|   Producer B   |\n'
          '|                |         |                |\n'
          '+----------------+         |                |\n'
          '                           +----------------+\n'
          '                           +----------------+\n'
          '                           |                |\n'
          '+----------------+         |                |\n'
          '|                |         |                |\n'
          '|   Consumer B   |-------->|   Producer A   |\n'
          '|                |         |                |\n'
          '+----------------+         |                |\n'
          '                           |                |\n'
          '                           +----------------+\n')


def run():
    c_a = Border(Padding(Paragraph(['Consumer A']), Size(2, 1), Size(2, 1)))
    c_b = Border(Padding(Paragraph(['Consumer B']), Size(2, 1), Size(2, 1)))
    p_a = Border(Padding(Paragraph(['Producer A']), Size(2, 3), Size(2, 3)))
    p_b = Border(Padding(Paragraph(['Producer B']), Size(2, 1), Size(2, 2)))
    model = Graph([c_a, c_b, p_a, p_b], [(c_b, p_a), (c_a, p_b)], layer_spacing=9)
    return model


if __name__ == '__main__':
    print(run().render())
