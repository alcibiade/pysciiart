from pysciiart.graph import Graph
from pysciiart.widget import Paragraph, Padding, Size, Border

result = ('+-------+                                             +-------+\n'
          '|       |                                             |       |\n'
          '|   A   |-------------------------------------------->|   D   |\n'
          '|       |    |    +-------+         +-------+    |    |       |\n'
          '+-------+    |    |       |         |       |    |    +-------+\n'
          '             +--->|   B   |-------->|   C   |----+\n'
          '                  |       |         |       |\n'
          '                  +-------+         +-------+\n')


def run():
    a = Border(Padding(Paragraph(['A']), Size(2, 1), Size(2, 1)))
    b = Border(Padding(Paragraph(['B']), Size(2, 1), Size(2, 1)))
    c = Border(Padding(Paragraph(['C']), Size(2, 1), Size(2, 1)))
    d = Border(Padding(Paragraph(['D']), Size(2, 1), Size(2, 1)))
    model = Graph([a, b, c, d], [(a, b), (b, c), (c, d), (a, d)], layer_spacing=9)
    return model


if __name__ == '__main__':
    print(run().render())
