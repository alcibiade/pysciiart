from pysciiart.graph import Graph
from pysciiart.widget import Paragraph, Padding, Size, Border

result = ('+--------------+                                    +----------------+\n'
          '|              |                                    |                |\n'
          '|   Consumer   |----------------------------------->|   Producer B   |\n'
          '|              |    |    +----------------+    |    |                |\n'
          '+--------------+    |    |                |    |    |                |\n'
          '                    |    |                |    |    +----------------+\n'
          '                    |    |                |    |\n'
          '                    +--->|   Producer A   |----+\n'
          '                         |                |\n'
          '                         |                |\n'
          '                         |                |\n'
          '                         +----------------+\n')


def run():
    c = Border(Padding(Paragraph(['Consumer']), Size(2, 1), Size(2, 1)))
    p_a = Border(Padding(Paragraph(['Producer A']), Size(2, 3), Size(2, 3)))
    p_b = Border(Padding(Paragraph(['Producer B']), Size(2, 1), Size(2, 2)))
    model = Graph([c, p_a, p_b], [(c, p_a), (p_a, p_b), (c, p_b)], layer_spacing=9)
    return model


if __name__ == '__main__':
    print(run().render())
