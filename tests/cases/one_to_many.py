from pysciiart.graph import Graph
from pysciiart.widget import Paragraph, Padding, Size, Border

result = ('                            +----------------+\n'
          '                            |                |\n'
          '+--------------+     +----->|   Producer A   |\n'
          '|              |     |      |                |\n'
          '|   Consumer   |-----+      +----------------+\n'
          '|              |     |      +----------------+\n'
          '+--------------+     |      |                |\n'
          '                     +----->|   Producer B   |\n'
          '                            |                |\n'
          '                            +----------------+\n')


def run():
    c = Border(Padding(Paragraph(['Consumer']), Size(2, 1), Size(2, 1)))
    p_a = Border(Padding(Paragraph(['Producer A']), Size(2, 1), Size(2, 1)))
    p_b = Border(Padding(Paragraph(['Producer B']), Size(2, 1), Size(2, 1)))
    model = Graph([c, p_a, p_b], [(c, p_a), (c, p_b)], layer_spacing=12)
    return model
