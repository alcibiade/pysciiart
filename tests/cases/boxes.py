from pysciiart.graph import Graph
from pysciiart.widget import Paragraph, Padding, Size, Border

result = ('+- Box 2 -----+\n'
          '|             |     +- Box 1 -----+\n'
          '|             |     |             |\n'
          '|   Hello     |---->|   Hello     |\n'
          '|   World !   |     |   World !   |\n'
          '|             |     |             |\n'
          '|             |     +-------------+\n'
          '+-------------+\n')


def run():
    b1 = Border(Padding(Paragraph(['Hello', 'World !']), Size(2, 1), Size(2, 1)), 'Box 1')
    b2 = Border(Padding(Paragraph(['Hello', 'World !']), Size(2, 2), Size(2, 2)), 'Box 2')
    model = Graph([b1, b2], [(b2, b1)])
    return model
