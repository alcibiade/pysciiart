from pysciiart.graph import Graph
from pysciiart.widget import Paragraph

result = '''\
World !-->Hello
'''


def run():
    w1 = Paragraph(['Hello'])
    w2 = Paragraph(['World !'])
    model = Graph([w1, w2], [(w2, w1)], layer_spacing=3)
    return model
