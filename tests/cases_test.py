import logging
import pkgutil
import unittest
from importlib import import_module


class GraphTests(unittest.TestCase):

    def setUp(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.info('Starting test cases')

    def tearDown(self):
        self.logger.info('End of test cases')

    def test_cases(self):
        module_name = 'cases'
        module = import_module(module_name)

        for module_info in pkgutil.iter_modules(module.__path__):
            module_fullname = f'{module_name}.{module_info.name}'
            self.logger.info(f'Running test case: {module_fullname}')

            submodule = import_module(module_fullname)

            self.assertTrue(hasattr(submodule, 'run'), f'Test case {module_fullname} must define a run() function')
            self.assertTrue(hasattr(submodule, 'result'), f'Test case {module_fullname} must define a result string')

            model = submodule.run()
            output = str(model.render())

            self.assertEqual(output, submodule.result, f'Test case {module_fullname} must return consistent raster')

            self.logger.info(f'Result:\n{output}')
