import unittest

from pysciiart.coordinates import Position, Path, PathStep


class CoordinatesTests(unittest.TestCase):

    def test_path_right_down(self):
        p1 = Position(0, 0)
        p2 = Position(10, 10)

        steps = Path(p1, p2)

        self.assertEqual(len(steps), 21)
        self.assertIn((PathStep.HORIZONTAL, Position(4, 0)), steps)
        self.assertIn((PathStep.VERTICAL, Position(5, 5)), steps)
        self.assertIn((PathStep.HORIZONTAL, Position(6, 10)), steps)

    def test_path_right_up(self):
        p1 = Position(0, 10)
        p2 = Position(10, 0)

        steps = Path(p1, p2)

        self.assertEqual(len(steps), 21)
        self.assertIn((PathStep.HORIZONTAL, Position(4, 10)), steps)
        self.assertIn((PathStep.VERTICAL, Position(5, 5)), steps)
        self.assertIn((PathStep.HORIZONTAL, Position(6, 0)), steps)


if __name__ == '__main__':
    unittest.main()
