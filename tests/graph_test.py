import unittest

from pysciiart.graph import Graph, LayerModel
from pysciiart.widget import *


class GraphTests(unittest.TestCase):

    def test_Graph_Empty(self):
        model = Graph([], [])
        # print(model.render())
        text = str(model.render())

        self.assertEqual('', text)

    def test_Graph_Single(self):
        w1 = Paragraph(['Hello'])
        model = Graph([w1], [])

        self.assertEqual(str(w1.render()), str(model.render()))

    def test_Graph_Unrelated(self):
        w1 = Paragraph(['Hello'])
        w2 = Paragraph(['World !'])
        model = Graph([w1, w2], [])

        self.assertEqual('Hello\nWorld !\n',
                         str(model.render()))

    def test_Graph_Simple_Dependency(self):
        w1 = Paragraph(['Hello'])
        w2 = Paragraph(['World !'])
        model = Graph([w1, w2], [(w1, w2)])

        self.assertEqual('Hello---->World !\n',
                         str(model.render()))

    def test_Graph_Inverted_Dependency(self):
        w1 = Paragraph(['Hello'])
        w2 = Paragraph(['World !'])
        model = Graph([w1, w2], [(w2, w1)])

        self.assertEqual('World !---->Hello\n',
                         str(model.render()))

    def test_Layer_Model(self):
        layers = LayerModel()
        self.assertEqual(layers.get_layers(), [[]])

        w_a = Paragraph(['a'])
        w_b = Paragraph(['b'])
        w_c = Paragraph(['c'])
        w_d = Paragraph(['d'])

        layers.add(w_a)
        layers.add(w_b)
        layers.add(w_c)

        self.assertEqual(layers.get_layers(), [[w_a, w_b, w_c]])
        self.assertEqual(layers.find_item_position(w_a), (0, 0))
        self.assertEqual(layers.find_item_position(w_b), (0, 1))
        self.assertEqual(layers.find_item_position(w_c), (0, 2))
        self.assertEqual(layers.find_item_position(w_d), None)

        layers.shift(w_b)

        self.assertEqual(layers.find_item_position(w_a), (0, 0))
        self.assertEqual(layers.find_item_position(w_b), (1, 0))
        self.assertEqual(layers.find_item_position(w_c), (0, 1))

    def test_Layer_Model_shift_nested(self):
        c_a = Border(Paragraph(['Content Item A1']), 'Child A')
        c_b = Border(Paragraph(['Content Item B1']), 'Child B')
        p1 = Border(VBox([c_a, c_b]), 'Parent 1')

        c_c = Border(Paragraph(['Content Item C1']), 'Child C')
        c_d = Border(Paragraph(['Content Item D1']), 'Child D')
        p2 = Border(VBox([c_c, c_d]), 'Parent 2')

        layers = LayerModel()
        layers.add(p1)
        layers.add(p2)
        layers.shift(c_d)

        self.assertEqual(layers.get_layers()[0], [p1])
        self.assertEqual(layers.get_layers()[1], [p2])


if __name__ == '__main__':
    unittest.main()
